""" A game, where the primary objective is to use the Ball to break as many bricks as possible.
    Powerups can augment abilities, giving score multipliers and extra lives (for example).
"""

from random import randint
from graphics import *

class Paddle:
    """ User controlled slab which deflects the Ball, thereby controlling the Ball. """
    def __init__(self):
        self.paddle_y = 500
        self.paddle = Rectangle(Point(350, self.paddle_y), Point(445, self.paddle_y + 20))
        self.paddle.setFill("White")

    def activate(self, window):
        """ Draws paddle. """
        self.paddle.draw(window)

    def move_with_mouse(self, window):
        """ Draws the paddle and grants the user control of it. Use within a loop. """
        location = window.winfo_pointerx()
        moved_paddle = location - self.paddle.getCenter().getX()
        self.paddle.move(moved_paddle, 0)

    def ball_hit_paddle(self, ball_object):
        """ Determines if the ball hit the paddle. """
        ball = ball_object.ball
        ball_width = ball_object.ball_width

        return bool(
            (ball.getCenter().getX() - ball_width) >= self.paddle.getP1().getX()
            and (ball.getCenter().getX() - ball_width) <= self.paddle.getP2().getX()
            and ball.getCenter().getY() == (self.paddle_y - ball_width))


class Ball:
    """ Small orb, controlled by the user via the paddle, which can hit and destroy bricks. """
    def __init__(self, lives, ball_width):
        self.x_per_second = 7
        self.y_per_second = -(randint(10, 15))
        self.ball_width = ball_width

        self.ball = Circle(Point(385, 485), self.ball_width)
        self.ball.setFill("White")
        self.life_counter = lives

    def activate(self, window):
        """ Draws the ball. """
        self.ball.draw(window)

    def move(self):
        """ Makes the ball move. Should be used in a loop. """
        self.ball.move(self.x_per_second, self.y_per_second)

    def bounce_off_x(self):
        """ Will cause the ball to move in the opposite horizontal direction. """
        self.x_per_second = -self.x_per_second

    def bounce_off_y(self):
        """ Will cause the ball to move in the opposite vertical direction. """
        self.y_per_second = -self.y_per_second

    def lose_life(self):
        """ Causes the ball to lose one of its lives and refresh the life counter display. """
        self.life_counter -= 1
        if self.life_counter <= 0:
            return False
        else:
            return True


class Brick:
    """ Must be destroyed at all costs using the Ball. """
    def __init__(self, x, y):
        colors = ['Green', 'Yellow', 'Red', 'Blue',
                  'Orange', 'Purple', 'Cyan',
                  'Green', 'Brown', 'Gray']

        self.brick = Rectangle(Point(x, y), Point(x + 80, y + 30))
        self.brick.setOutline(colors[randint(0, 9)])

    def activate(self, window):
        """ Place the brick on the specified window. """
        self.brick.draw(window)

    def deactivate(self):
        """ Undraw brick. """
        self.brick.undraw()

    def ball_hit_brick(self, ball_object):
        """ Check whether or not the brick was hit by the ball. """
        ball = ball_object.ball

        brick_point1_x = self.brick.getP1().getX()
        brick_point1_y = self.brick.getP1().getY()
        brick_point2_x = self.brick.getP2().getX()
        brick_point2_y = self.brick.getP2().getY()

        return bool(
            brick_point1_x <= ball.getCenter().getX() - ball_object.ball_width
            and brick_point2_x >= ball.getCenter().getX() - ball_object.ball_width
            and brick_point2_y >= ball.getCenter().getY() - ball_object.ball_width
            and brick_point1_y >= ball.getCenter().getY() - ball_object.ball_width)

class Powerup:
    """ When collected, this will augment the Ball's abilities in many different ways. """
    def __init__(self, power):
        self.power = power

        self.power_list = ["Extra Life", "Multi-Ball", "Score Multiplier"]
        if self.power not in self.power_list:
            raise ValueError('Powerup not found.')

        x_spawn_point = randint(400, 600)

        self.powerup = Circle(Point(x_spawn_point, 185), 20)
        self.powerup.setFill("Orange")


    def activate(self, window):
        """ Draws the powerup to the screen. """
        self.powerup.draw(window)



def make_window(max_window_x, max_window_y):
    """ Creates the window and what it contains. """
    main_window = GraphWin("The One and Only Neon Brick Breaker™ © Milo Gilad 2002-2102",
                           max_window_x, max_window_y)
    main_window.setBackground("Black")

    return main_window

def make_bricks():
    """ Create 60 bricks, to be drawn to the screen. """
    brick_list = []
    brick_x = 0
    brick_y = 45
    for columm in range(6):
        for row in range(10):
            brick_list.append(Brick(brick_x, brick_y))
            brick_x += 80
        brick_x = 0
        brick_y += 30
    return brick_list

def draw_bricks_to_window(window, brick_list):
    """ Draw supplied bricks to the specified window. """
    for brick in brick_list:
        brick.activate(window)


def main():
    """ Must be executed to coherently run the program. """
    max_window_x = 800
    max_window_y = 600
    window = make_window(max_window_x, max_window_y)

    brick_list = make_bricks()
    draw_bricks_to_window(window, brick_list)

    paddle = Paddle()
    paddle.activate(window)

    ball_object = Ball(10, 15)
    ball_object.activate(window)

    while True:
        paddle.move_with_mouse(window)
        ball_object.move()

        if (ball_object.ball.getCenter().getX() > max_window_x
                or ball_object.ball.getCenter().getX() < 0):
            ball_object.bounce_off_x()

        if (ball_object.ball.getCenter().getY() < 0
                or ball_object.ball.getCenter().getY() > max_window_y):
            ball_object.bounce_off_y()

        if paddle.ball_hit_paddle(ball_object):
            ball_object.bounce_off_y()

        for index, brick in enumerate(brick_list):
            if brick.ball_hit_brick(ball_object):
                brick.deactivate()
                brick_list.pop(index)
                ball_object.bounce_off_x()
                ball_object.bounce_off_y()

main()
